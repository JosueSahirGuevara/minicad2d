
package graficador;

import com.jogamp.opengl.GLCapabilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.*;

public class Panel extends JPanel{
    
    JPanel panelBotones;
    JPanel panelEtiqueta;
    JButton btnCircLleno,btnCuadLleno,btnRecLleno,btnRomboLleno,trianguloLleno;
    JButton circuloVacio,cuadradoVacio,pentagonoVacio,romboVacio,trianguloVacio;
    JButton botonIzquierda,botonDerecha,btnEscalarXP,btnEscalarXN,btnEscalarYP,btnEscalarYN;
    JButton btnBorrar;
    PanelPintar panelPintar;
    GLCapabilities capabilities;
    
    
    public int selected;
    
    public Panel(GLCapabilities capabilities){
        this.capabilities = capabilities;
        init();
        
    }
    
    public void init() {
        this.setLayout(new BorderLayout());
        
        
        
       
        
        panelBotones = new JPanel();
        panelBotones.setLayout(new GridLayout(9, 1, 10, 10));  
        panelBotones.setBackground(Color.GRAY);
        
        panelPintar = new PanelPintar(capabilities);
        panelPintar.setSize(100, 700);
        
        
        btnCircLleno = new JButton("Circulo LLeno");
        btnCircLleno.setBackground(Color.BLUE);
        btnCircLleno.setName("cirL");
        
        btnCuadLleno = new JButton("Cuadrado LLeno");
        btnCuadLleno.setName("cuadL");
        btnCuadLleno.setBackground(Color.MAGENTA);
        
        btnRecLleno = new JButton("Pentagono LLeno");
        btnRecLleno.setName("recL");
        btnRecLleno.setBackground(Color.GREEN);
        
        btnRomboLleno = new JButton("Rombo LLeno");
        btnRomboLleno.setName("romL");
        btnRomboLleno.setBackground(Color.ORANGE);
        
        trianguloLleno = new JButton("Triangulo LLeno");
        trianguloLleno.setName("triL");
        trianguloLleno.setBackground(Color.YELLOW);
        
        circuloVacio = new JButton("Circulo Vacio");
        circuloVacio.setName("cirV");
        circuloVacio.setBackground(Color.BLUE);
        
        cuadradoVacio = new JButton("Cuadrado Vacio");
        cuadradoVacio.setName("cuadV");
        cuadradoVacio.setBackground(Color.MAGENTA);
        
        pentagonoVacio = new JButton("Pentagono Vacio");
        pentagonoVacio.setName("recV");
        pentagonoVacio.setBackground(Color.GREEN);
        
        romboVacio = new JButton("Rombo Vacio");
        romboVacio.setName("romV");
        romboVacio.setBackground(Color.ORANGE);
        
        trianguloVacio = new JButton("Triangulo Vacio");
        trianguloVacio.setName("triV");
        trianguloVacio.setBackground(Color.YELLOW);
        
        botonIzquierda = new JButton("<-- Rotar");
        botonIzquierda.setName("rotIzq");
        botonIzquierda.setBackground(Color.lightGray);
         
        btnEscalarXP = new JButton("X+");
        btnEscalarXP.setName("escXP");
        btnEscalarXP.setBackground(Color.lightGray);
        
        botonDerecha = new JButton("Rotar -->");
        botonDerecha.setName("rotDer");
        botonDerecha.setBackground(Color.lightGray);
       
        btnEscalarXN = new JButton("X-");
        btnEscalarXN.setName("escXN");
        btnEscalarXN.setBackground(Color.lightGray);
        
        
        btnEscalarYP = new JButton("Y+");
        btnEscalarYP.setName("escYP");
        btnEscalarYP.setBackground(Color.lightGray);
        
        btnEscalarYN = new JButton("Y-");
        btnEscalarYN.setName("escYN");
        btnEscalarYN.setBackground(Color.lightGray);
        
        btnBorrar = new JButton("Borrar figura");
        btnBorrar.setName("borrar");
        btnBorrar.setBackground(Color.RED);
        
        
        panelBotones.add(btnCuadLleno);
        panelBotones.add(cuadradoVacio);
        panelBotones.add(btnCircLleno);
        panelBotones.add(circuloVacio);        
        panelBotones.add(btnRecLleno);
        panelBotones.add(pentagonoVacio);
        panelBotones.add(trianguloLleno);
        panelBotones.add(trianguloVacio);
        panelBotones.add(btnRomboLleno);
        panelBotones.add(romboVacio);        
        panelBotones.add(botonIzquierda);
        panelBotones.add(botonDerecha);
        panelBotones.add(btnEscalarXP);
        panelBotones.add(btnEscalarXN);
        panelBotones.add(btnEscalarYP);
        panelBotones.add(btnEscalarYN);
        panelBotones.add(btnBorrar);
        
        panelPintar.setBackground(Color.cyan);
        this.add(panelBotones,BorderLayout.WEST);
        this.add(panelPintar,BorderLayout.CENTER);
        
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
    }
    
    public void addEventos(Controlador c) {
        panelPintar.addMouseMotionListener(c);
        panelPintar.addMouseListener(c);
        this.panelPintar.addEventos(c);
        
        circuloVacio.addActionListener(c);
        btnCircLleno.addActionListener(c);
        cuadradoVacio.addActionListener(c);
        btnCuadLleno.addActionListener(c);
        pentagonoVacio.addActionListener(c);
        btnRecLleno.addActionListener(c);
        trianguloVacio.addActionListener(c);
        trianguloLleno.addActionListener(c);
        romboVacio.addActionListener(c);
        btnRomboLleno.addActionListener(c);
        botonIzquierda.addActionListener(c);
        botonDerecha.addActionListener(c);
        btnEscalarXP.addActionListener(c);
        btnEscalarXN.addActionListener(c);
        btnEscalarYP.addActionListener(c);
        btnEscalarYN.addActionListener(c);
        
        btnBorrar.addActionListener(c);
    }
}


