/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficador;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.JComponent;
import javax.swing.table.DefaultTableModel;
import modelos.*;

public class Controlador implements ActionListener, MouseMotionListener, MouseListener, KeyListener {

    Panel panel;

    public Controlador(Panel panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComponent c = (JComponent) e.getSource();
        String opt = c.getName();
        int ex, ey;
        float rotarGrados;
        float escalarX, escalarY;
        float r,g,b;
        
        float v = (float)(Math.random()*100);
        r = ((int)v)%2;
        
        v = (float)(Math.random()*100);
        g = ((int)v)%2;
        
        v = (float)(Math.random()*100);
        b = ((int)v)%2;
        
        // si los 3 son igual a 1 eso quiere decir
        // se dibujara en blanco y como el fondo es blanco no se va a ver
        // asi que se dibuja en negro
        if(r==1 && g==1 && b==1){
            r = 0;
            g = 0;
            b = 0;
        }

        switch (opt) {
            case "cirL":
                panel.panelPintar.forma = new CirculoLleno(180, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "cirV":
                panel.panelPintar.forma = new CirculoVac(180, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "cuadL":
                panel.panelPintar.forma = new CuadradoLleno(4, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "cuadV":
                panel.panelPintar.forma = new CuadradoVacio(4, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                
                break;
            case "recL":
                panel.panelPintar.forma = new PentagonoLleno(4, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "recV":
                panel.panelPintar.forma = new PentagonoVacio(5, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "triL":
                panel.panelPintar.forma = new TrianguloLleno(3, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "triV":
                panel.panelPintar.forma = new TrianguloVacio(3, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "romL":
                panel.panelPintar.forma = new RomboLleno(4, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "romV":
                panel.panelPintar.forma = new RomboVacio(4, 0, 100, 200, 100,r,g,b);
                panel.panelPintar.formas.add(panel.panelPintar.forma);
                break;
            case "rotIzq":
                rotarGrados = panel.panelPintar.formas.get(panel.selected).getGrados();
                panel.panelPintar.formas.get(panel.selected).setGrados(rotarGrados - 25);
                break;
            case "rotDer":
                rotarGrados = panel.panelPintar.formas.get(panel.selected).getGrados();
                panel.panelPintar.formas.get(panel.selected).setGrados(rotarGrados + 25);
                break;
            case "escXP":
                escalarX = panel.panelPintar.formas.get(panel.selected).getXS();
                panel.panelPintar.formas.get(panel.selected).setXS(escalarX + 1);
                break;
            case "escXN":
                escalarX = panel.panelPintar.formas.get(panel.selected).getXS();
                
                if (escalarX > 1) {
                    panel.panelPintar.formas.get(panel.selected).setXS(escalarX - 1);
                }
                break;
            case "escYP":
                escalarY = panel.panelPintar.formas.get(panel.selected).getYS();
                panel.panelPintar.formas.get(panel.selected).setYS(escalarY + 1);
                break;
            case "escYN":

                escalarY = panel.panelPintar.formas.get(panel.selected).getYS();
                if (escalarY > 1) {
                    panel.panelPintar.formas.get(panel.selected).setYS(escalarY - 1);
                }

                break;
            case "borrar":
                panel.panelPintar.formas.remove(panel.selected);
                break;
        }

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int xe = e.getX();
        int ye = e.getY();
        panel.panelPintar.formas.get(panel.selected).setX(xe);
        panel.panelPintar.formas.get(panel.selected).setY(ye);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int xe = e.getX();
        int ye = e.getY();

        for (int i = 0; i < panel.panelPintar.formas.size(); i++) {
            if (panel.panelPintar.formas.get(i).checa(xe, ye)) {
                System.out.println("le diste clic a la figura!!!");
                panel.selected = i;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
        
        switch (e.getKeyChar()) {
            case 'n':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(0, 0, 0);
                break;
            case 'r':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(1, 0, 0);
                break;
            case 'v':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(0, 1, 0);
                break;
            case 'a':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(0, 0, 1);
                break;
            case 'y':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(1, 1, 0);
                break;
            case 'm':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(1, 0, 1);
                break;
            case 'c':
                panel.panelPintar.formas.get(panel.selected).cambiaColor(0, 1, 1);
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

}
